const express = require ('express');
const bodyParser = require('body-parser');
const app = express ();
app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json());
const port = 8000;

app.listen(port, ()=>{
    console.log("Projeto executando na porta: " + port);
});

app.get ('/clientes', (req,res)=>{
    let source = req.query;
    let ret = "Nome:" + source.nome;
    res.send("{message:" +ret+ "}");
});

app.post('/clientes',(req, res)=>{
    let dados = req.body;
    let headers_ = req.headers["access"];
    let msg
    if(headers_ == 1234){
        msg = 'acesso concedido a proxima etapa';
        res.send("{message:"+ dados.nome+" "+msg+ "}");
    }else{
        msg = 'acesso negado';
        res.send("{message:"+msg+"}");
    }
    
    
});

app.get ('/funcionarios', (req,res)=>{
    let source = req.query;
    let ret = "Funcionario:" + source.funcionario;
    res.send("{message:" +ret+ "}");
});

app.delete('/funcionarios/:valor',(req, res)=>{
    let dados = req.params.valor;
    let headers_ = req.headers["access"];
    let msg;
    if(headers_ == 54321){
        msg = 'acesso concedido a proxima etapa';
        res.send("{message:"+ dados+" "+msg+ "}");
    }else{
        msg = 'acesso negado';
        res.send("{message:" +msg+ "}");
    }
});

app.put('/funcionarios',(req,res)=>{
    let dados = req.body;
    let headers_ = req.headers["access"];
    let msg
    if(headers_ == 17){
        msg = 'acesso concedido a proxima etapa';
        res.send("{message:"+ dados.nome+" "+msg+ "}");
    }else{
        msg = 'acesso negado';
        res.send("{message:" +msg+ "}");
    }
})